#include <stdint.h>
#include <Wire.h>
#include "heartrate_3.h"
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>


const char * ssid = "YYY";
const char * password = "xxx";
const char * host = "10.0.1.111";

const uint16_t port = 11000;



ESP8266WiFiMulti WiFiMulti;
WiFiClient client;
  
const int AFE_RST_PIN = 12;  
const int AFE_RDY_PIN = 14;
const int LED_PIN = 0;

long timeStamp = millis();
volatile bool isAdcReady = false;

void setup()
{
  Serial.begin(115200);
  Serial.println("UART Initialized");

  pinMode(LED_PIN, OUTPUT);
  pinMode(AFE_RDY_PIN, INPUT_PULLUP);
  
  AFE4404Setup();     // GPIO / HeartRate 3 / UART / I2C Setups
  wifiSetup();
  interruptSetup();
  delay(200);
}

void loop(){
  if(isAdcReady){
    isAdcReady = false;
    led_values_t * vals = hr3_get_values();
    statHRMAlgo( (*vals).led1_val );
    timedEvents();
    yield();     
    if(isAdcReady){
      led_values_t * vals = hr3_get_values();   
    } 
    isAdcReady = false;
  } 
}

void timedEvents(){
  if(millis() - timeStamp > 200){
    sendRate();    
    digitalWrite(LED_PIN, !digitalRead(LED_PIN));
    timeStamp = millis();
  }  
}

void sendRate(){
  noInterrupts();
  bool isClientConnected = client.connected();
  interrupts();
  
  if(isClientConnected){
    uint16_t rate = hr3_get_heartrate();                                   // Gets current Heartrate
    String output = String(rate) + '\n';
    noInterrupts();
    client.print(output);
    interrupts();
    Serial.println(rate);
//    delay(100);      
  } 
  else {
    Serial.println("disconnected!");
    noInterrupts();
    client.stop();
    wifiSetup();
    wifiConnect();
    interrupts();
  }
}

void wifiSetup(){
  delay(10);

  // We start by connecting to a WiFi network
  WiFiMulti.addAP(ssid, password);

  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");

  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);
}

void wifiConnect(){
  Serial.print("connecting to ");
  Serial.println(host);

  client.stop();
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    Serial.println("wait...");
    delay(500);
//    return;
  }  
  else {
    Serial.println("connected");  
  }
}

void AFE4404Setup( void ){
  //Local Declarations
  char            text[20] = { 0 };
  dynamic_modes_t dynamic_modes;
  uint8_t         address = 0x58;

  //  RST_DIR = 1;
  pinMode(AFE_RST_PIN, OUTPUT);
  digitalWrite(AFE_RST_PIN, HIGH);

  //Set up dynamic modes for Heart Rate 3 Initialization
  dynamic_modes.transmit = trans_en;                       //Transmitter disabled
  dynamic_modes.curr_range = led_norm;                     //LED range 0 - 100
  dynamic_modes.adc_power = adc_on;                        //ADC on
  dynamic_modes.clk_mode = osc_mode;                       //Use internal Oscillator
  dynamic_modes.tia_power = tia_off;                       //TIA off
  dynamic_modes.rest_of_adc = rest_of_adc_off;             //Rest of ADC off
  dynamic_modes.afe_rx_mode = afe_rx_normal;               //Normal Receiving on AFE
  dynamic_modes.afe_mode = afe_normal;                     //Normal AFE functionality

  // TWI
  Wire.begin(4, 5);
  //  Wire.begin();
  delay(500);
  Serial.println( "TWI Initialized" );

  //Toggle Reset pin
  digitalWrite(AFE_RST_PIN, LOW);
  delayMicroseconds(50);
  digitalWrite(AFE_RST_PIN, HIGH);

  //Heart Rate 3 Initialize
  hr3_init( address, &dynamic_modes );
  Serial.println( "HR3 Initialized" );

  initStatHRM();      // Initializes values to 0
  delay(100);
}

void interruptSetup( void ){
  attachInterrupt(AFE_RDY_PIN, adc_rdy_isr, RISING);
}

void adc_rdy_isr(){
  isAdcReady = true;
}
