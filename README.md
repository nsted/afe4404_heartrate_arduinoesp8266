This is the firmware for an ESP8266 to read from 
MikroElectronica's "Heart Rate 3 Click" board. 
It was used to create a wireless biometric wristband.

To use it:
1. add your Wifi credentials to the code and reupload to the ESP8266. 
2. connect to the ESP8266 as a TCP server on the same local network.
3. ESP8266 will begin streaming heart rate values.