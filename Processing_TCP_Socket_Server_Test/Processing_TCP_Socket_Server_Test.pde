import processing.net.*;

Server s;
Client c;
String input;
String output;
int data[];
PrintWriter printWriter;
long timeStamp = 0;

void setup() 
{
  size(450, 255);
  background(204);
  stroke(0);
  frameRate(5); // Slow it down a little
  s = new Server(this, 11000); // Start a simple server on a port
  printWriter = createWriter("output.txt");
}

void draw() 
{
  // Receive data from client
  c = s.available();
  if (c != null) {
    input = c.readString();
    print(input);
    println(" @ " + hour() + ":" + minute() + ":" + second());
    long timeDiff = millis()/1000 - timeStamp;
    if(timeDiff > 2) {
      output = timeDiff + "second pause @ " + hour() + ":" + minute() + ":" + second();
      println('\t' + output);
      printWriter.println(output);
    }
    timeStamp = millis()/1000;    
  }
}

void keyPressed() {
  printWriter.flush(); // Writes the remaining data to the file
  printWriter.close(); // Finishes the file
  exit(); // Stops the program
}

void stop() {
  printWriter.flush(); // Writes the remaining data to the file
  printWriter.close(); // Finishes the file
  exit(); // Stops the program  
}